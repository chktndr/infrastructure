output "kube_config_raw" {
  value     = azurerm_kubernetes_cluster.cluster.kube_config_raw
  sensitive = true
}

output "kube_config" {
  value     = azurerm_kubernetes_cluster.cluster.kube_config.0
  sensitive = true
}

output "cluster_name" {
  value = azurerm_kubernetes_cluster.cluster.name
}
