variable "name" {
  description = "The name of the cluster"
  type        = string
}

variable "location" {
  description = "The resource location"
  type        = string
}

variable "rg" {
  description = "Resource group name"
  type        = string
}

variable "dns_prefix" {
  description = "The cluster DNS prefix"
  type        = string
}

variable "node_count" {
  description = "The number of nodes in the cluster"
  type        = number
  default     = 1
}

variable "vm_size" {
  description = "The VM size of each node"
  type        = string
}

variable "kubernetes_version" {
  description = "The version of Kubernetes to use"
  type        = string
  default     = "1.15.7"
}

variable "service_principal_id" {
  description = "The service principal client id"
  type        = string
}

variable "service_principal_secret" {
  description = "The service principal client secret/password"
  type        = string
}

variable "vnet" {
  description = "The vnet where nodes should be attached"
  type        = any
}

variable "node_subnet" {
  description = "The subnet where nodes should be attached"
  type        = any
}

variable "service_cidr" {
  description = "The subnet CIDR used for k8s services"
  type        = string
}

variable "docker_bridge_cidr" {
  description = "CIDR block for the docker bridge network"
  type        = string
  default     = "172.17.0.1/16"
}

variable "load_balancer_sku" {
  description = "The SKU of the Azure Load Balancer to use"
  type        = string
  default     = "Basic"
}

variable "tags" {
  description = "Resource tags"
  type        = map
  default = {
  }
}
