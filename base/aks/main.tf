resource "azurerm_kubernetes_cluster" "cluster" {
  name                = var.name
  location            = var.location
  resource_group_name = var.rg
  dns_prefix          = var.dns_prefix
  node_resource_group = "${var.rg}-nodes"
  kubernetes_version  = var.kubernetes_version

  default_node_pool {
    name           = "nodepool"
    node_count     = var.node_count
    vm_size        = var.vm_size
    vnet_subnet_id = var.node_subnet.id
  }

  service_principal {
    client_id     = var.service_principal_id
    client_secret = var.service_principal_secret
  }

  role_based_access_control {
    enabled = true
  }

  addon_profile {
    aci_connector_linux {
      enabled = false
    }
    azure_policy {
      enabled = false
    }
    http_application_routing {
      enabled = false
    }
    oms_agent {
      enabled = false
    }
    kube_dashboard {
      enabled = false
    }
  }

  network_profile {
    network_plugin     = "azure"
    network_policy     = "azure"
    load_balancer_sku  = var.load_balancer_sku
    docker_bridge_cidr = var.docker_bridge_cidr
    service_cidr       = var.service_cidr
    dns_service_ip     = cidrhost(var.service_cidr, 4)
  }

  tags = var.tags
}
