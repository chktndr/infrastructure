output "public_ip" {
  description = "The created public IP value"
  value       = azurerm_public_ip.ip.ip_address
}

output "acr_login_server" {
  description = "The login server for the created registry"
  value       = module.acr.login_server
}

output "dns_name_servers" {
  description = "The DNS nameservers"
  value       = module.dns.name_servers
}

output "resource_group_name" {
  description = "The name of the main resource group"
  value       = azurerm_resource_group.rg.name
}

output "storage_account_name" {
  description = "The name of the storage account"
  value       = azurerm_storage_account.storage_account.name
}

output "aks_cluster_name" {
  description = "The name of the AKS cluster"
  value       = module.aks.cluster_name
}
