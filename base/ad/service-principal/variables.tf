variable "name" {
  description = "The name of the service principal"
  type        = string
}

variable "password_validity_duration" {
  description = "The length of time the password is valid for"
  type        = string
  default     = "8760h"
}

variable "password_length" {
  description = "Length of password"
  type        = number
  default     = 20
}

variable "key_vault_name" {
  description = "Name of the key vault where password should be saved"
  type        = string
}

variable "key_vault_rg" {
  description = "Resource group containing the key vault"
  type        = string
}

variable "roles" {
  description = "The role assignments for the service principal"
  type = list(object({
    scope     = string
    role_name = string
  }))
}

variable "tags" {
  description = "Resource tags"
  type        = map
  default = {
  }
}
