output "service_principal_id" {
  value = azuread_service_principal.sp.application_id
}

output "service_principal_password" {
  value     = azuread_service_principal_password.password.value
  sensitive = true
}
