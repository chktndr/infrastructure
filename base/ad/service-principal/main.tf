provider "random" {
}

data "azurerm_key_vault" "vault" {
  name                = var.key_vault_name
  resource_group_name = var.key_vault_rg
}

resource "azuread_application" "app" {
  name = var.name
}

resource "azuread_service_principal" "sp" {
  application_id = azuread_application.app.application_id
}

resource "random_password" "password" {
  length  = var.password_length
  special = true
}

resource "azuread_service_principal_password" "password" {
  service_principal_id = azuread_service_principal.sp.id
  value                = random_password.password.result
  end_date_relative    = var.password_validity_duration
}

resource "azurerm_key_vault_secret" "password" {
  name         = "${var.name}-password"
  value        = azuread_service_principal_password.password.value
  key_vault_id = data.azurerm_key_vault.vault.id

  tags = var.tags
}

resource "azurerm_role_assignment" "role" {
  count                            = length(var.roles)
  scope                            = var.roles[count.index].scope
  role_definition_name             = var.roles[count.index].role_name
  principal_id                     = azuread_service_principal.sp.object_id
  skip_service_principal_aad_check = true
}
