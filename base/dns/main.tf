resource "azurerm_dns_zone" "zone" {
  name                = var.zone_name
  resource_group_name = var.rg

  tags = var.tags
}

resource "azurerm_dns_a_record" "record" {
  count = length(var.a_records)

  name                = var.a_records[count.index].name
  zone_name           = azurerm_dns_zone.zone.name
  resource_group_name = var.rg
  ttl                 = var.a_record_ttl
  records             = [var.a_records[count.index].value]
}
