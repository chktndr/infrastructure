output "name_servers" {
  description = "The DNS nameservers"
  value       = azurerm_dns_zone.zone.name_servers
}
