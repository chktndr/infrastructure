variable "rg" {
  description = "Resource group name"
  type        = string
}

variable "zone_name" {
  description = "The name of the DNS zone"
  type        = string
}

variable "a_records" {
  description = "List of DNS A records"
  type = list(object({
    name  = string
    value = string
  }))
}

variable "a_record_ttl" {
  description = "TTL of A records"
  type        = number
  default     = 1800
}

variable "tags" {
  description = "Resource tags"
  type        = map
  default = {
  }
}
