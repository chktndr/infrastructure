variable "name" {
  description = "The name of the virtual network"
  type        = string
}

variable "location" {
  description = "The resource location"
  type        = string
}

variable "rg" {
  description = "Resource group name"
  type        = string
}

variable "address_space" {
  description = "Network address space"
  type        = string
}

variable "subnets" {
  description = "CIDR blocks for subnets"
  type        = list(string)
}

variable "service_endpoints" {
  description = "List of Microsoft service endpoints to enable on all subnets"
  type        = list(string)
}

variable "tags" {
  description = "Resource tags"
  type        = map
  default = {
  }
}
