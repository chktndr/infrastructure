resource "azurerm_virtual_network" "vnet" {
  name                = var.name
  location            = var.location
  resource_group_name = var.rg
  address_space       = [var.address_space]
  tags                = var.tags
}

resource "azurerm_network_security_group" "nsg" {
  name                = "${var.name}-nsg"
  resource_group_name = var.rg
  location            = var.location
  tags                = var.tags
}

resource "azurerm_subnet" "subnet" {
  count                = length(var.subnets)
  name                 = "${azurerm_virtual_network.vnet.name}_${count.index}"
  resource_group_name  = var.rg
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefix       = var.subnets[count.index]
  service_endpoints    = var.service_endpoints
}

resource "azurerm_subnet_network_security_group_association" "nsg_association" {
  count                     = length(var.subnets)
  subnet_id                 = azurerm_subnet.subnet[count.index].id
  network_security_group_id = azurerm_network_security_group.nsg.id
}
