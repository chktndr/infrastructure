output "vnet" {
  description = "The vnet object"
  value       = azurerm_virtual_network.vnet
}

output "subnets" {
  description = "List of subnets"
  value       = azurerm_subnet.subnet.*
}
