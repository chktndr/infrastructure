output "login_server" {
  description = "Registry login server"
  value       = azurerm_container_registry.acr.login_server
}

output "id" {
  description = "Resource id for the ACR"
  value       = azurerm_container_registry.acr.id
}
