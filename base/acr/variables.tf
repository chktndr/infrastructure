variable "rg" {
  description = "Resource group name"
  type        = string
}

variable "name" {
  description = "Registry name"
  type        = string
}

variable "location" {
  description = "Resource location"
  type        = string
}

variable "tags" {
  description = "Resource tags"
  type        = map
  default = {
  }
}
