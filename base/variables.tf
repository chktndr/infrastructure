variable "prefix" {
  description = "The resource name prefix"
  type        = string
}

variable "location" {
  description = "Resource location"
  type        = string
}

variable "cluster_node_count" {
  description = "Number of nodes in the AKS cluster"
  type        = number
  default     = 1
}

variable "cluster_node_size" {
  description = "Size of each cluster node VM"
  type        = string
  default     = "Standard_B2s"
}

variable "vnet_address_space" {
  description = "The virtual network address space"
  type        = string
}

variable "subnets" {
  description = "The subnets within the vnet"
  type        = list(string)
}

variable "aks_service_address_space" {
  description = "The AKS service network address space"
  type        = string
}

variable "key_vault_name" {
  description = "Name of the key vault where passwords should be saved"
  type        = string
}

variable "key_vault_rg" {
  description = "Resource group containing the key vault"
  type        = string
}

variable "dns_zone_name" {
  description = "The name of the DNS zone"
  type        = string
}

variable "tags" {
  description = "Resource tags"
  type        = map
  default = {
  }
}
