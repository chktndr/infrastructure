terraform {
  backend "azurerm" {
    resource_group_name  = "chktndr-tfstate"
    storage_account_name = "chktndrtfstate"
    container_name       = "tfstate"
    key                  = "base/terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}

provider "azuread" {}

locals {
  rg        = "${var.prefix}-${terraform.workspace}"
  acr_name  = "${var.prefix}${terraform.workspace}registry"
  aks_name  = "${local.rg}-cluster"
  vnet_name = "${local.rg}-vnet"
  ip_name   = "${local.rg}-ip"
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg
  location = var.location
  tags     = var.tags
}

module "acr" {
  source = "./acr"

  name     = local.acr_name
  rg       = azurerm_resource_group.rg.name
  location = var.location
  tags     = var.tags
}

module "vnet" {
  source = "./vnet"

  name              = local.vnet_name
  location          = var.location
  rg                = azurerm_resource_group.rg.name
  address_space     = var.vnet_address_space
  subnets           = var.subnets
  service_endpoints = ["Microsoft.Storage"]
  tags              = var.tags
}

module "cluster_service_principal" {
  source = "./ad/service-principal"

  name           = local.aks_name
  key_vault_name = var.key_vault_name
  key_vault_rg   = var.key_vault_rg

  roles = [{
    scope     = module.acr.id
    role_name = "AcrPull"
    }, {
    scope     = azurerm_resource_group.rg.id
    role_name = "Contributor"
  }]

  tags = var.tags
}

module "aks" {
  source = "./aks"

  name       = local.aks_name
  location   = var.location
  rg         = azurerm_resource_group.rg.name
  dns_prefix = local.aks_name
  node_count = var.cluster_node_count
  vm_size    = var.cluster_node_size

  service_principal_id     = module.cluster_service_principal.service_principal_id
  service_principal_secret = module.cluster_service_principal.service_principal_password

  vnet         = module.vnet.vnet
  node_subnet  = module.vnet.subnets[0]
  service_cidr = var.aks_service_address_space

  tags = var.tags
}

resource "azurerm_public_ip" "ip" {
  name                = local.ip_name
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
  sku                 = "Basic"

  tags = var.tags
}

resource "azurerm_storage_account" "storage_account" {
  name                     = replace("${local.rg}storage", "-", "")
  location                 = var.location
  resource_group_name      = azurerm_resource_group.rg.name
  account_tier             = "Standard"
  account_kind             = "Storage"
  account_replication_type = "LRS"

  tags = var.tags
}

resource "azurerm_storage_account_network_rules" "storage_network_rules" {
  resource_group_name  = azurerm_resource_group.rg.name
  storage_account_name = azurerm_storage_account.storage_account.name

  default_action             = "Deny"
  virtual_network_subnet_ids = module.vnet.subnets.*.id
}

module "dns" {
  source = "./dns"

  rg        = azurerm_resource_group.rg.name
  zone_name = var.dns_zone_name

  a_records = [{
    name  = "@"
    value = azurerm_public_ip.ip.ip_address
    }, {
    name  = "www"
    value = azurerm_public_ip.ip.ip_address
  }]

  tags = var.tags
}
