prefix                    = "chktndr"
location                  = "canadacentral"
cluster_node_count        = 1
cluster_node_size         = "Standard_B2s"
vnet_address_space        = "10.2.0.0/16"
subnets                   = ["10.2.0.0/16"]
aks_service_address_space = "10.3.0.0/16"
key_vault_name            = "chktndr-tfstate-kv"
key_vault_rg              = "chktndr-tfstate"
dns_zone_name             = "chickentendermarketplace.ca"
tags = {
  app = "chickentendermarketplace"
  env = "production"
}
