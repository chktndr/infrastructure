prefix                    = "chktndr"
location                  = "canadacentral"
cluster_node_count        = 2
cluster_node_size         = "Standard_B2s"
vnet_address_space        = "10.0.0.0/16"
subnets                   = ["10.0.0.0/16"]
aks_service_address_space = "10.1.0.0/16"
key_vault_name            = "chktndr-tfstate-kv"
key_vault_rg              = "chktndr-tfstate"
dns_zone_name             = "chktndrmkt.tk"
tags = {
  app = "chickentendermarketplace"
  env = "development"
}
