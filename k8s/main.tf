terraform {
  backend "azurerm" {
    resource_group_name  = "chktndr-tfstate"
    storage_account_name = "chktndrtfstate"
    container_name       = "tfstate"
    key                  = "k8s/terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}

data "terraform_remote_state" "infra" {
  backend   = "azurerm"
  workspace = terraform.workspace

  config = {
    resource_group_name  = "chktndr-tfstate"
    storage_account_name = "chktndrtfstate"
    container_name       = "tfstate"
    key                  = "base/terraform.tfstate"
  }
}

data "azurerm_kubernetes_cluster" "cluster" {
  name                = data.terraform_remote_state.infra.outputs.aks_cluster_name
  resource_group_name = data.terraform_remote_state.infra.outputs.resource_group_name
}

provider "kubernetes" {
  load_config_file       = false
  host                   = data.azurerm_kubernetes_cluster.cluster.kube_config.0.host
  username               = data.azurerm_kubernetes_cluster.cluster.kube_config.0.username
  password               = data.azurerm_kubernetes_cluster.cluster.kube_config.0.password
  client_certificate     = base64decode(data.azurerm_kubernetes_cluster.cluster.kube_config.0.client_certificate)
  client_key             = base64decode(data.azurerm_kubernetes_cluster.cluster.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(data.azurerm_kubernetes_cluster.cluster.kube_config.0.cluster_ca_certificate)
}

resource "kubernetes_storage_class" "azure_file_storage" {
  metadata {
    name = "azure-disk-retained"
  }
  storage_provisioner = "kubernetes.io/azure-disk"
  reclaim_policy      = "Retain"
  parameters = {
    storageAccount = data.terraform_remote_state.infra.outputs.storage_account_name
    skuName        = "Standard_LRS"
  }
}

resource "kubernetes_namespace" "namespace" {
  metadata {
    labels = {
      env = terraform.workspace
    }

    name = "chktndr-${terraform.workspace}"
  }
}

resource "kubernetes_secret" "config" {
  depends_on = [kubernetes_namespace.namespace]

  metadata {
    name      = "chktndr-config"
    namespace = "chktndr-${terraform.workspace}"
    labels = {
      env = terraform.workspace
    }
  }

  data = {
    "config.json" = var.chktndr_config
  }

  type = "Opaque"
}
